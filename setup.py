import numpy as np
from scipy.stats import gamma
import utilities.uniform_lattice as uniform_lattice
from utilities.helper_funcs import get_color_list
from scipy.stats import bernoulli
from utilities.random_waypoint import RWM
from AP import AP
from USER import User


class SetUp:
    def __init__(self, simulation):
        self.simulation = simulation
        self.ap_list = []
        self.user_list = []
        self.blocking_flag = [1 if simulation.block_prob_one or simulation.block_prob_two else 0]
        self.count_color_id = 0
        self.num_wifi_aps = 1
        self.id_to_ap_mapping = {}
        self.room_size = []

    def create_users_aps_setup(self, simulation):
        if self.simulation.scenario == "plane":
            self.create_lifi_ap_plane_objects(simulation)
            self.create_wifi_ap_plane_objects()
            self.create_users_plane_objects()
        elif self.simulation.scenario == 'overlap':
            self.create_a_list_with_lifi_ap_overlap_topo()
            self.create_a_list_with_wifi_ap_objects()
            self.create_a_list_with_users_random_within_a_range(hardcordered=False)
        else:
            raise NameError('No such scenario')

        self.get_user_id()
        self.get_ap_id()
        self.create_demands()

        if simulation.mobility_flag and simulation.speed != 0:
            for user in self.user_list:
                user.color = color_list[user.my_id]

            x_range = self.room_size[0:2]
            y_range = self.room_size[2:]
            # print(x_range, y_range)
            for user in self.user_list:
                rwp = RWM(x_range, y_range, simulation.speed)
                rwp.update_postion_rwm(user, simulation.number_of_iterations)
                user.x = user.x_mobility[0]
                user.y = user.y_mobility[0]

    def create_demands(self):
        d = {15515: [15, 5, 15], 162116: [16, 21, 16], 25155: [25, 15, 5], 1551: [15, 5, 1], 2555: [25, 5, 5],
             25255: [25, 25, 5], 252525: [25, 25, 25], 151515: [15, 15, 15], 555: [5, 5, 5], 1555: [15, 5, 5]}
        demands = d[self.simulation.demands]
        for i in range(len(demands)):
            demands[i] = int(demands[i]) * 10 ** 6
        demands = demands * int(len(self.user_list) / 1.5)
        # print("Demands are {}".format(demands[:3]))
        for user in self.user_list:
            user.demand = demands[user.my_id]

    def rx_orientation(self, controller, mean_elevation, time_step):
        if mean_elevation == 'random_once' and time_step > 0:
            return
        elif mean_elevation == '0_or_-41':
            for user in controller.user_list:
                user.elevation_angle = np.random.choice([0, -41])
                print("Rx orientation {}".format(user.elevation_angle))
                user.elevation_angle = user.elevation_angle * np.pi / 180

        elif mean_elevation == 'random' or mean_elevation == 'random_once':
            for user in controller.user_list:
                user.elevation_angle = np.random.choice(list(range(-90, 90)))
                print("Rx orientation {}".format(user.elevation_angle))
                user.elevation_angle = user.elevation_angle * np.pi / 180
        else:
            for user in controller.user_list:
                mean_elevation = int(mean_elevation)
                user.elevation_angle = mean_elevation + np.random.choice(list(range(-10, 10)))
                # print("Rx orientation {}".format(user.elevation_angle))
                user.elevation_angle = user.elevation_angle * np.pi / 180

    def rx_orientation_fixed(self, controller, mean_elevation, time_step):
        if mean_elevation == 'random_once' and time_step > 0:
            return
        elif mean_elevation == '0_or_-41':
            for user in controller.user_list:
                user.elevation_angle = np.random.choice([0, -41])
                user.elevation_angle = user.elevation_angle * np.pi / 180

        elif mean_elevation == 'random' or mean_elevation == 'random_once':
            for user in controller.user_list:
                user.elevation_angle = np.random.choice(list(range(-90, 90)))
                user.elevation_angle = user.elevation_angle * np.pi / 180
        else:
            for user in controller.user_list:
                mean_elevation = int(mean_elevation)
                user.elevation_angle = mean_elevation * np.pi / 180

    def blockages_our_model_with_2_prob(self, controller, block_prob_one, block_prob_two):
        num_blocked = 0
        if block_prob_one or block_prob_two:
            for user in controller.user_list:
                if user.blocked:
                    bern_res = bernoulli.rvs(size=1, p=block_prob_two)[0]
                else:
                    bern_res = bernoulli.rvs(size=1, p=block_prob_one)[0]
                if bern_res == 1:
                    num_blocked += 1
                    user.blocked = True
                    user.my_blockages.append(1)
                    #print("User {} is blocked".format(user.my_id))
                else:
                    user.blocked = False
                    user.my_blockages.append(0)
        return num_blocked

    def blockages_our_model_with_1_prob(self, controller, block_prob_one):
        num_blocked = 0
        if block_prob_one:
            for user in controller.user_list:
                bern_res = bernoulli.rvs(size=1, p=block_prob_one)[0]
                if bern_res == 1:
                    num_blocked += 1
                    user.blocked = True
                    user.my_blockages.append(1)
                    print("User {} is blocked".format(user.my_id))
                else:
                    user.blocked = False
                    user.my_blockages.append(0)
        return num_blocked

    def blockages_with_ocupation_rate(self, controller, number_of_time_steps):
        if self.blocking_flag:
            for user in controller.user_list:
                shape = 1
                scale = 10/4  # should be 10 per min -> 10 per 120 time steps -> 10/3 per for 30 time steps
                occurrence_rate = np.random.gamma(shape, scale, 1)
                delta = np.random.uniform(0, 1, 1)
                if delta > occurrence_rate:
                    user.blocked = True
                    user.blockage_duration = int(np.random.uniform(1, number_of_time_steps, 1))
                    print("Uses {} -> blockage duration {}".format(user.my_id, user.blockage_duration))

    def get_user_id(self):
        for user in self.user_list:
            user.my_id = self.user_list.index(user) + 1

    def get_ap_id(self):
        for ap in self.ap_list:
            ap.my_id = self.ap_list.index(ap) + 1
            self.id_to_ap_mapping[ap.my_id] = ap

    def create_wifi_ap_plane_objects(self):
        wifi_ap_x_list = [0]
        wifi_ap_y_list = [self.simulation.num_rows/2+0.5]  # +0.5 just to avoid lines overlap
        for i in range(len(wifi_ap_x_list)):
            ap = AP("wifi", wifi_ap_x_list[i], wifi_ap_y_list[i], color_list[self.count_color_id])
            self.count_color_id += 2
            self.ap_list.append(ap)

    def create_a_list_with_users_random_within_a_range(self, hardcordered):
        # overlap topo
        if hardcordered:
            x_users = [-3, 0, 1, -1.3, -4, -3,    1, 0.5,  0, -3, -3.5, -2.5]
            y_users = [0, 1, -2.5, -3, -2.5, -1, -1, 0, -4, -4, 1, 1]

        else:
            x_users = []
            y_users = []
            for i in range(self.simulation.num_user_overlap):
                x = np.random.uniform(self.room_size[0], self.room_size[1], 1)
                y = np.random.uniform(self.room_size[2], self.room_size[3], 1)
                x_users.append(x)
                y_users.append(y)

        for i in range(len(x_users)):
            user = User(x_users[i], y_users[i], None)
            self.user_list.append(user)

    def create_a_list_with_lifi_ap_overlap_topo(self):
        overlap_ratio_vs_shift = {6: 0.4, 16: 0.8, 30: 1.2, 48: 1.5, 77: 1.9, 98: 2.1}
        shift = overlap_ratio_vs_shift[16] # 16
        r = 1.8
        overlap_topo = uniform_lattice.OverlapTopo(r, shift, self.simulation.num_lifi_aps_overlap)
        lifi_aps_x, lifi_aps_y = overlap_topo.generate_topo(shift)
        x_min = y_min = np.inf
        x_max = y_max = -np.inf
        for i in range(len(lifi_aps_x)):
            ap = AP("lifi", lifi_aps_x[i], lifi_aps_y[i], color_list[self.count_color_id])
            self.ap_list.append(ap)
            if x_min > lifi_aps_x[i]:
                x_min = lifi_aps_x[i]
            if x_max < lifi_aps_x[i]:
                x_max = lifi_aps_x[i]

            if y_min > lifi_aps_y[i]:
                y_min = lifi_aps_y[i]
            if y_max < lifi_aps_y[i]:
                y_max = lifi_aps_y[i]
        self.room_size = [x_min-r, x_max+r, y_min-r, y_max+r]

    def create_lifi_ap_plane_objects(self, simulation):
        y_max = (1.1 / 2 + 0.81 * self.simulation.num_rows) * 0.95
        # print(y_max)
        lifi_aps_x_left = 1
        if simulation.ap_every_num_rows == 1:
            lifi_aps_y = [1.1 / 2 + 0.81 * x for x in range(0, self.simulation.num_rows)]
        else:
            lifi_aps_y = [1.1 / 2 + 0.81 * x * simulation.ap_every_num_rows for x in
                          range(0, int(self.simulation.num_rows / simulation.ap_every_num_rows) + 1)]
        for i in range(len(lifi_aps_y)):
            if lifi_aps_y[i] > y_max:
                continue
            ap = AP("lifi", lifi_aps_x_left, lifi_aps_y[i], color_list[self.count_color_id])
            self.ap_list.append(ap)
            ap = AP("lifi", -lifi_aps_x_left, lifi_aps_y[i], color_list[self.count_color_id])
            self.ap_list.append(ap)

    def create_users_plane_objects(self):
        x_users = []
        y_users = []
        x_lines = [0.57, 1, 1.43]
        y_lines = [0.81*x + 0.5 for x in range(0, self.simulation.num_rows)]

        for i in range(0, len(y_lines)):
            for j in range(0, len(x_lines)):
                x_users.append(x_lines[j])
                y_users.append(y_lines[i])
                x_users.append(-x_lines[j])
                y_users.append(y_lines[i])

        for i in range(len(x_users)):
            user = User(x_users[i], y_users[i], None)
            self.user_list.append(user)

    def create_a_list_with_wifi_ap_objects(self):
        x = (self.room_size[1] + self.room_size[0])/2
        y = (self.room_size[3] + self.room_size[2])/2
        wifi_ap_x_list = [x]
        wifi_ap_y_list = [y]
        for i in range(len(wifi_ap_x_list)):
            ap = AP("wifi", wifi_ap_x_list[i], wifi_ap_y_list[i], color_list[self.count_color_id])
            self.ap_list.append(ap)


color_list = get_color_list()

