from simulation import Simulation
from utilities.helper_funcs import block_print
from utilities.helper_funcs import enable_print
import numpy as np
import process_res_simple
import process_results_full
scenario = 'overlap'
count_files = 0

# parameters
number_of_iterations = 50
num_simulations = 1
mobility_flag = 0
speed = 0  # 0, 1, 5 -1:varying speed
plot_or_not = 0
save_plot = 0
num_lifi_aps_overlap = 4  # must be an even number
# possible demand configurations
# d = {15515: [15, 5, 15], 162116: [16, 21, 16], 25155: [25, 15, 5], 1551: [15, 5, 1], 2555: [25, 5, 5],
#     25255: [25, 25, 5], 252525: [25, 25, 25], 151515: [15, 15, 15], 555: [5, 5, 5], 1555: [15, 5, 5]}
demands = [15515]
algorithms = ['GT_WO_DEMAND']
# To block or enable print
block_printing = True

varying_rx_orientation = 0
mean_elevations = [0]  # 0, -41, 'random'
block_model = 2  # 0, 1 or 2
block_prob_one = 0.1
block_prob_two = 0.2
num_users = [18]

for algorithm in algorithms:
    for demand in demands:
        for num_user_overlap in num_users:
            for mean_elevation in mean_elevations:
                for vho_cost in [0, 0.4]:  # vertical handover efficiency
                    if vho_cost != 0 and algorithm not in ['SMART', 'GT_WO_DEMAND']:
                        print(f"Skipping for algo {algorithm} with vho={vho_cost}")
                        continue
                    print(f"OVERLAP ****************overhead = {vho_cost}****************")
                    if vho_cost == 0:
                        hho_cost = 0

                    else:
                        hho_cost = 0.6
                    print(f"num_iter={number_of_iterations}, Rx:{varying_rx_orientation} angle={mean_elevation}, "
                          f"mobility:{mobility_flag} speed={speed} block model {block_model} "
                          f"blockages {block_prob_one}, {block_prob_two}, alg {algorithm}, demand = {demand}")
                    if block_printing:
                        block_print()
                    for sim_num in range(num_simulations):
                        count_files += 1
                        np.random.seed(sim_num + 3681362)
                        sim = Simulation(number_of_iterations, scenario, 0, mobility_flag,
                                                           vho_cost, varying_rx_orientation,
                                                           plot_or_not, sim_num, mean_elevation,
                                                           speed, demand, hho_cost, block_prob_one,
                                                           block_prob_two, num_user_overlap, num_lifi_aps_overlap,
                                                           algorithm, save_plot, 0, block_model)
                        sim.main()

enable_print()
print("Number of sim files should be {}".format(count_files))
process_res_simple.main('overlap')
# process_results_full.main('overlap', 'num_users', num_users)
